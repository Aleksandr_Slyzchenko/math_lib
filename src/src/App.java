public class App {
    public static void main(String [] args){
        Task1(100,40);
        Task2(50,80,15,1);
        Task4(2.0);
    }
    public static void Task1(double speed, double alpha){
        double s = ((speed * speed) / 9.8) * Math.sin(Math.toRadians(2 * alpha));
        System.out.println("Снаряд пролетить таку відстань " + s);
    }
    public static void Task2(int v1,int v2, int S, int t){
        double res= Math.abs(S-((v1+v2)*t));
        System.out.println("Відстань між автомобілями " + res);
    }
    public static void Task4(double x){
        double exp = 2.7182;
        double z = (6*Math.log(Math.sqrt(Math.pow(exp,x + 1)+ 2*Math.pow(exp,x)*Math.cos(x)))) / (Math.log(x - Math.pow(exp,x - 1)*Math.sin(x))) + Math.abs(Math.cos(x) / Math.pow(exp,Math.sin(x)));
        System.out.println("Ваше значення " + z);
    }
}
